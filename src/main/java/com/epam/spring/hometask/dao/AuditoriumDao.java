package com.epam.spring.hometask.dao;

import com.epam.spring.hometask.domain.Auditorium;

import java.util.HashMap;
import java.util.Map;

public class AuditoriumDao implements IDao {

    private static Map<String, Auditorium> auditoriumMap = new HashMap<>();

    @Override
    public void create(String id, Object o) {
        auditoriumMap.put(id, (Auditorium) o);
    }

    @Override
    public Object read(String id) {
        return auditoriumMap.get(id);
    }

    @Override
    public void update(String id, Object o) {
        auditoriumMap.put(id, (Auditorium) o);
    }

    @Override
    public void delete(String id) {
        auditoriumMap.remove(id);
    }

    @Override
    public Map<String, Auditorium> readAll() {
        return auditoriumMap;
    }
}
