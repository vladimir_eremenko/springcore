package com.epam.spring.hometask.dao;

import com.epam.spring.hometask.domain.Event;

import java.util.HashMap;
import java.util.Map;


public class EventDao implements IDao {

    private static Map<String, Event> eventMap = new HashMap<>();

    public EventDao() {
    }

    @Override
    public void create(String id, Object o) {
        eventMap.put(id, (Event) o);
        ((Event) o).setId(Long.valueOf(id));

    }

    @Override
    public Object read(String id) {
        return eventMap.get(id);
    }

    @Override
    public void update(String id, Object o) {
        eventMap.put(id, (Event) o);
    }

    @Override
    public void delete(String id) {
        eventMap.remove(id);
    }

    @Override
    public Map<String, Event> readAll() {
        return eventMap;
    }

}
