package com.epam.spring.hometask.dao;

import com.epam.spring.hometask.domain.DomainObject;

import java.util.Map;

public class DomainObjectDao implements IDao {

    private static Map<String, DomainObject> doMap;

    @Override
    public void create(String id, Object o) {
        doMap.put(id, (DomainObject) o);
    }

    @Override
    public Object read(String id) {
        return doMap.get(id);
    }

    @Override
    public void update(String id, Object o) {
        doMap.put(id, (DomainObject) o);
    }

    @Override
    public void delete(String id) {
        doMap.remove(id);
    }

    @Override
    public Map<String, ? extends Object> readAll() {
        return doMap;
    }

}
