package com.epam.spring.hometask.dao;

import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.Ticket;

import java.time.LocalDateTime;
import java.util.Set;

public interface IBookingDao {

    void create(Ticket ticket);
    Set<Ticket> readAll(Event event, LocalDateTime localDateTime);
    int getCountBookedDao();
}
