package com.epam.spring.hometask.dao;

import com.epam.spring.hometask.domain.Ticket;

import java.util.HashMap;
import java.util.Map;

public class TicketDao implements IDao {

    private static Map<String, Ticket> ticketMap = new HashMap<>();

    public TicketDao() {
    }

    @Override
    public void create(String id, Object o) {
        ticketMap.put(id, (Ticket) o);
    }

    @Override
    public Object read(String id) {
        return ticketMap.get(id);
    }

    @Override
    public void update(String id, Object o) {
        ticketMap.put(id, (Ticket) o);
    }

    @Override
    public void delete(String id) {
        ticketMap.remove(id);
    }

    @Override
    public Map<String, Ticket> readAll() {
        return ticketMap;
    }

}
