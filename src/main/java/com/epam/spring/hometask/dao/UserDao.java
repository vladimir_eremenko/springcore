package com.epam.spring.hometask.dao;

import com.epam.spring.hometask.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.HashMap;
import java.util.Map;

public class UserDao implements IDao {

    private static Map<String, User> userMap = new HashMap<>();

    public UserDao() {
    }

    @Override
    public void create(String id, Object o) {
        userMap.put(id, (User) o);
    }

    @Override
    public Object read(String id) {
        return userMap.get(id);
    }

    @Override
    public void update(String id, Object o) {
        userMap.put(id, (User) o);
    }

    @Override
    public void delete(String id) {
        userMap.remove(id);
    }

    @Override
    public Map<String, User> readAll() {
        return userMap;
    }

}
