package com.epam.spring.hometask.dao;

import java.util.Map;

public interface IDao {

    void create(String id, Object o);
    Object read(String id);
    void update(String id, Object o);
    void delete(String id);
    Map<String, ? extends Object> readAll();
}
