package com.epam.spring.hometask.dao;

import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class BookingDao implements IBookingDao {

   private Map<String, Ticket> ticketBooked = new HashMap<>();

    public void create(Ticket ticket) {
        ticketBooked.put(String.valueOf(ticket.getId()), ticket);
    }

    public Set<Ticket> readAll(Event event, LocalDateTime localDateTime) {
        return ticketBooked.entrySet().stream()
                .filter(element -> element.getValue().getEvent()
                .equals(event) && element.getValue().getDateTime()
                .equals(localDateTime)).map(Map.Entry::getValue).collect(Collectors.toSet());
    }

    @Override
    public int getCountBookedDao() {
        return ticketBooked.size();
    }
}
