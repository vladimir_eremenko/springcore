package com.epam.spring.hometask;

import com.epam.spring.hometask.context.AppContext;
import com.epam.spring.hometask.dao.UserDao;
import com.epam.spring.hometask.domain.Auditorium;
import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.EventRating;
import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.service.beans.BookingServiceBean;
import com.epam.spring.hometask.service.beans.UserServiceBean;
import com.epam.spring.hometask.service.interfaces.BookingService;
import com.epam.spring.hometask.service.interfaces.EventService;
import com.epam.spring.hometask.service.interfaces.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppContext.class);
        UserService userService = (UserService) context.getBean("getUserServiceBean");
        EventService eventService = (EventService) context.getBean("getEventServiceBean");
        BookingService bookingService = (BookingService) context.getBean("getBookingServiceBean");


        Scanner scanner = new Scanner(System.in);

        while (true) {
            User user = new User();
            System.out.print("\n1 - create user\n2 - get user\n3 - list all users\n4 - show all events\n" +
                    "5 - create events\n6 - get ticket price\n7 - view purchased tickets\n9 - buy tickets\n0 - exit app\n");
            System.out.print("\n--> ");
            int choice = scanner.nextInt();
            if (choice == 1) {
                enterUserRegistrationInfo(user);
                if (!userService.save(user).equals(new User())) {
                    System.out.println("User registered!");
                }
                else {
                    System.out.println("User already exists!");
                }
            }
            else if (choice == 2) {
                useUser(scanner, user);
                User u = userService.getUserByEmail(user.getEmail());
                if (u != null) {
                    System.out.println(u.toString());
                    getOperations(u.getFirstName());
                    break;
                }
                else {
                    System.out.println("user is not found");
                }
            }
            else if (choice == 4) {
                System.out.println(eventService.getAll());
            }
            else if (choice == 5) {
                do {
                    System.out.println("example : name(meeting_room)/basePrice(12,5)/rating(LOW,MID,HIGH)/LocalDateTime:auditorium; .. ");
                    System.out.println("LocalDateTime example : 2018-5-11-9-40");
                    System.out.println("Auditorium example : meeting_room-125-7,8,9");
                    eventService.save(getEvent());
                } while (!scanner.next().equals("exit"));
            }
            else if (choice == 3) {
                System.out.println(userService.getAll());
            }
            else if (choice == 6) {
                String request = scanner.next();
                Event event  = new Event();
               // System.out.println(bookingService.getTicketsPrice(event));
            }
            else if (choice == 7) {
                System.out.println(userService.getAll());
            }
            else if (choice == 0) {
                System.out.println("app is closed");
                System.exit(0);
            }
            else {
                System.out.println("you entered not correct info");
            }
        }
    }




    private static void enterUserRegistrationInfo(User user) {
        System.out.println("FirstName/LastName/Email");
        String[] elements = new Scanner(System.in).next().split("/");
        user.setFirstName(elements[0]);
        user.setLastName(elements[1]);
        user.setEmail(elements[2]);

    }

    private static void useUser(Scanner scanner, User user) {
        System.out.print("Enter Email : ");
        String email = scanner.next();
        user.setEmail(email);
    }


    private static Event getEvent() {
        Event event = new Event();
        String element = new Scanner(System.in).next();
        String[] elements = getElement(element, "/");
        event.setName(elements[0]);
        event.setBasePrice(Double.parseDouble(elements[1]));
        event.setRating(EventRating.valueOf(elements[2]));
        event.setAirDates(getNavigableSetLocalDateTime(elements[3]));
        event.setAuditoriums(getNavigableMapAuditorium(elements[3]));
        return event;
    }

    private static void getOperations(String userName) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println();
            System.out.println("Actions\n1 - create tickets\n2 - log out\n");
            int choose = scanner.nextInt();
            if (choose == 1) {

            }
            else if (choose == 2) {

            }
            else if (choose == 3) {

            }
        }
    }

    private static String[] getElement(String element, String regex) {
        return element.split(regex);
    }

    private static NavigableSet<LocalDateTime> getNavigableSetLocalDateTime(String element) {
        return Arrays.stream(element.split(";")).map(App::getLocalDateTime)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    private static NavigableMap<LocalDateTime, Auditorium> getNavigableMapAuditorium(String auditorium) {
        String[] elements = auditorium.split(";");
        return Arrays.stream(elements)
                .collect(Collectors.toMap(App::getLocalDateTime, App::getAuditorium, (a, b) -> b, TreeMap::new));
    }

    private static LocalDateTime getLocalDateTime(String element) {
        String[] elements = element.split(":");
        String[] date = elements[0].split("-");
        return LocalDateTime.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]),
                Integer.parseInt(date[2]), Integer.parseInt(date[3]), Integer.parseInt(date[4]));
    }

    private static Auditorium getAuditorium(String element) {
        String[] elements = element.split(":");
        String[] au = elements[1].split("-");
        Auditorium auditorium = new Auditorium();
        auditorium.setName(au[0]);
        auditorium.setNumberOfSeats(Long.parseLong(au[1]));
        auditorium.setVipSeats(getSetVipSeats(au[2]));
        return auditorium;
    }

    private static Set<Long> getSetVipSeats(String element) {
        return Arrays.stream(element.split(","))
                .map(Long::valueOf).collect(Collectors.toSet());
    }
}
