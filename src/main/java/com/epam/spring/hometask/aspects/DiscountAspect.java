package com.epam.spring.hometask.aspects;

import com.epam.spring.hometask.domain.User;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Aspect
@EnableAspectJAutoProxy
public class DiscountAspect {

    private static Map<String, Integer> countsUserDiscounts = new HashMap<>();
    private static long countDiscounts;

    @Pointcut("execution(* com.epam.spring.hometask.service.beans.DiscountStrategy.getDiscount(..))")
    private void discountMethod() {
    }

    @AfterReturning(pointcut = "discountMethod()", returning = "retVal")
    public void getCountDiscount(JoinPoint joinPoint, Object retVel) {
        User user = (User) joinPoint.getArgs()[0];
        if (countsUserDiscounts.containsKey(user.getEmail())) {
            countsUserDiscounts.put(user.getEmail(), countsUserDiscounts.get(user.getEmail()) + 1);
        } else {
            countsUserDiscounts.put(user.getEmail(), 1);
        }
        countDiscounts++;
        System.out.println("User count get discount : " + countsUserDiscounts.get(user.getEmail()));
        System.out.println("Count all discounts : " + countDiscounts);
    }
}
