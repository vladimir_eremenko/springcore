package com.epam.spring.hometask.aspects;

import com.epam.spring.hometask.dao.BookingDao;
import com.epam.spring.hometask.domain.Ticket;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


@Aspect
@EnableAspectJAutoProxy
public class CounterAspect {

    private static Map<String, Integer> eventNameCounter = new HashMap<>();
    private static Map<String, Integer> eventBockedCounter = new HashMap<>();
    private static Map<String, Integer> eventPriceCounter = new HashMap<>();

    @Autowired
    @Qualifier("getBookingDao")
    private BookingDao bookingDAO;

    @Pointcut("execution(* com.epam.spring.hometask.service.beans.EventServiceBean.getByName(..))")
    private void getEventByNmae() {
    }

    @Pointcut("execution(* com.epam.spring.hometask.service.beans.BookingServiceBean.bookTickets(..)")
    private void getEventTicketBocked() {
    }

    @Pointcut("execution(* com.epam.spring.hometask.service.beans.BookingServiceBean.getTicketsPrice(..))")
    private void getEventPrices() {
    }

    @After("getEventByName()")
    public void countGetEventByName(JoinPoint joinPoint) {
        String event = (String) joinPoint.getArgs()[0];
        eventNameCounter.put(event, eventNameCounter.containsKey(event) ? eventNameCounter.get(event) + 1 : 1);
        System.out.println("Event " + event + "  was accessed by name " + eventNameCounter.get(event) + " times");
    }

    @After("getEventTicketBocked()")
    public void countGetTicketBocked(JoinPoint joinPoint) {
        Set<Ticket> tickets = (Set<Ticket>) joinPoint.getArgs()[0];
        tickets.forEach(ticket -> eventBockedCounter.entrySet().forEach(element -> {
            if (ticket.getUser().getEmail().equals(element.getKey())) {
                element.setValue(element.getValue() + 1);
            }
            else {
                eventBockedCounter.put(ticket.getUser().getEmail(), 1);
            }
        }));
        eventBockedCounter.forEach((key, value) ->
                System.out.println("user email : " + key + ", count bocked : " + value));
    }

    @AfterReturning(value = "getEventPrices()")
    public void getEventPrices(JoinPoint joinPoint) {
        String event = (String) joinPoint.getArgs()[0];
        eventPriceCounter.put(event, eventPriceCounter.containsKey(event) ? eventPriceCounter.get(event) + 1 : 1);
        System.out.println("Event " + event + "  was accessed ticket prices " + eventPriceCounter.get(event) + " times");
    }
}
