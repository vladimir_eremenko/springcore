package com.epam.spring.hometask.aspects;

import com.epam.spring.hometask.domain.Ticket;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import java.util.HashMap;
import java.util.Map;


@Aspect
@EnableAspectJAutoProxy
public class LuckyWinnerAspect {

    private static Map<String, Integer> countsUserLucky = new HashMap<>();

    @Pointcut("execution(* com.epam.spring.hometask.service.beans.BookingService.checkLucky(..))")
    private void luckyWinnerMethod(){
    }

    @AfterReturning(pointcut = "discountMethod()", returning = "retVal")
    public void getCountDiscount(JoinPoint joinPoint, Object retVel) {
        Ticket ticket = (Ticket) joinPoint.getArgs()[0];
        if((boolean) retVel){
            if (countsUserLucky.containsKey(ticket.getUser().getEmail())) {
                countsUserLucky.put(ticket.getUser().getEmail(), countsUserLucky.get(ticket.getUser().getEmail()) + 1);
            } else {
                countsUserLucky.put(ticket.getUser().getEmail(), 1);
            }
        }
        System.out.println("User : " + ticket.getUser().getFirstName() + " is lucky " + countsUserLucky.get(ticket.getUser().getEmail()) + " times");
    }
}
