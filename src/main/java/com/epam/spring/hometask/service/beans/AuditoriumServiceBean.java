package com.epam.spring.hometask.service.beans;

import com.epam.spring.hometask.dao.IDao;
import com.epam.spring.hometask.domain.Auditorium;
import com.epam.spring.hometask.service.interfaces.AuditoriumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

public class AuditoriumServiceBean implements AuditoriumService {

    @Autowired
    @Qualifier("getAuditoriumDao")
    private IDao dao;

    @Nonnull
    @Override
    public Set<Auditorium> getAll() {
        return (Set<Auditorium>) dao.readAll().values();
    }

    @Nullable
    @Override
    public Auditorium getByName(@Nonnull String name) {
        return (Auditorium) dao.read(name);
    }

}
