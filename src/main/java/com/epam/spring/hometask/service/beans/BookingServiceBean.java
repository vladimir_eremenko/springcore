package com.epam.spring.hometask.service.beans;

import com.epam.spring.hometask.dao.BookingDao;
import com.epam.spring.hometask.dao.IDao;
import com.epam.spring.hometask.domain.Auditorium;
import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.Ticket;
import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.service.interfaces.BookingService;
import com.epam.spring.hometask.service.interfaces.DiscountService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

public class BookingServiceBean implements BookingService {

    private static final double LOW_RATING_COEF = 0.85;
    private static final double MID_RATING_COEF = 1.0;
    private static final double HIGH_RATING_COEF = 1.25;
    private static final double VIP_SEATS_COEF = 2.25;


    @Autowired
    @Qualifier("getBookingDao")
    private BookingDao bookingDAO;


    @Autowired
    @Qualifier("getDiscountServiceBean")
    private DiscountService discountService;


    public BookingServiceBean() {

    }

    @Override
    public double getTicketsPrice(@Nonnull Event event, @Nonnull LocalDateTime dateTime,
                                  @Nullable User user, @Nonnull Set<Long> seats) {
        double sum = 0;
        Auditorium auditoriums = event.getAuditoriums().get(dateTime);
        double basePrice = event.getBasePrice();
        for (Long seat : seats) {
            double ticketPrice = basePrice;
            if (auditoriums.getVipSeats().contains(seat)) {
                ticketPrice *= VIP_SEATS_COEF;
            }
            switch (event.getRating()) {
                case LOW:
                    ticketPrice *= LOW_RATING_COEF;
                    break;
                case MID:
                    ticketPrice *= MID_RATING_COEF;
                    break;
                case HIGH:
                    ticketPrice *= HIGH_RATING_COEF;
                    break;
            }
            sum += ticketPrice;
        }
        sum = sum - (sum / 100) * discountService.getDiscount(user, event, dateTime, seats.size());
        return sum;
    }

    @Override
    public void bookTickets(@Nonnull Set<Ticket> tickets) {
        for (Ticket ticket : tickets) {
            ticket.getUser().getTickets().add(ticket);
            if (checkLucky(ticket)) {
                ticket.getEvent().setBasePrice(0);
            }
            bookingDAO.create(ticket);
        }
    }

    @Nonnull
    @Override
    public Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDateTime dateTime) {
        return bookingDAO.readAll(event, dateTime);
    }

    @Override
    public boolean checkLucky(Ticket ticket) {
        return ticket.getSeat() % 3 != 0;
    }
}
