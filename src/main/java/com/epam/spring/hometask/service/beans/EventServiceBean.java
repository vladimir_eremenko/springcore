package com.epam.spring.hometask.service.beans;

import com.epam.spring.hometask.dao.IDao;
import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.service.interfaces.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import java.util.Collection;

public class EventServiceBean implements EventService {

    @Autowired
    @Qualifier("getEventDao")
    private IDao eventDao;

    @Nullable
    @Override
    public Event getByName(@Nonnull String name) {
        return (Event) eventDao.read(getHashCodeEvent(name));
    }

    @Override
    public Event save(@Nonnull Event object) {
        String id = getHashCodeEvent(object.getName());
        eventDao.create(id, object);
        return (Event) eventDao.read(String.valueOf(id));
    }

    @Override
    public void remove(@Nonnull Event object) {
        eventDao.delete(getHashCodeEvent(object.getName()));
    }

    @Override
    public Event getById(@Nonnull Long id) {
        return (Event) eventDao.read(String.valueOf(id));
    }

    @Nonnull
    @Override
    public Collection<Event> getAll() {
        return (Collection<Event>) eventDao.readAll().values();
    }

    private String getHashCodeEvent(String name) {
        Event event = new Event();
        event.setName(name);
        return String.valueOf(event.hashCode());
    }

}
