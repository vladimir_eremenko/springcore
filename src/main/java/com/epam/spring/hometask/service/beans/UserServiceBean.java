package com.epam.spring.hometask.service.beans;

import com.epam.spring.hometask.dao.IDao;
import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

public class UserServiceBean implements UserService {

    @Autowired
    @Qualifier("getUserDao")
    private IDao userIDao;

    @Nullable
    @Override
    public User getUserByEmail(@Nonnull String email) {
        String id = getHashCodeUser(email);
        return (User) userIDao.read(id);
    }

    @Override
    public User save(@Nonnull User object) {
        String id = getHashCodeUser(object.getEmail());
        if (userIDao.read(id) == null) {
            userIDao.create(id, object);
            return (User) userIDao.read(id);
        }
        return new User();
    }

    @Override
    public void remove(@Nonnull User object) {
        userIDao.delete(getHashCodeUser(object.getEmail()));
    }

    @Override
    public User getById(@Nonnull Long id) {
        return (User) userIDao.read(String.valueOf(id));
    }

    @Nonnull
    @Override
    public Collection<User> getAll() {
        return (Collection<User>) userIDao.readAll().values();
    }

    private String getHashCodeUser(String email) {
        User user = new User();
        user.setEmail(email);
        return String.valueOf(user.hashCode());
    }

}
