package com.epam.spring.hometask.service.beans;

import com.epam.spring.hometask.dao.IDao;
import com.epam.spring.hometask.domain.DomainObject;
import com.epam.spring.hometask.service.interfaces.AbstractDomainObjectService;

import javax.annotation.Nonnull;
import java.util.Collection;

public class DomainObjectServiceBean implements AbstractDomainObjectService {

    private IDao dao;

    @Override
    public DomainObject save(@Nonnull DomainObject object) {
        String id = getHashCodeDomainObject(object.getId());
        dao.create(id, object);
        return (DomainObject) dao.read(id);
    }

    @Override
    public void remove(@Nonnull DomainObject object) {
        dao.delete(getHashCodeDomainObject(object.getId()));
    }

    @Override
    public DomainObject getById(@Nonnull Long id) {
        return (DomainObject) dao.read(getHashCodeDomainObject(id));
    }

    @Nonnull
    @Override
    public Collection getAll() {
        return dao.readAll().values();
    }

    private String getHashCodeDomainObject(Long id) {
        DomainObject domainObject = new DomainObject();
        domainObject.setId(id);
        return String.valueOf(domainObject.hashCode());
    }

    public IDao getDao() {
        return dao;
    }

    public void setDao(IDao dao) {
        this.dao = dao;
    }
}
