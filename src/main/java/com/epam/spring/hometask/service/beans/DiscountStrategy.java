package com.epam.spring.hometask.service.beans;

import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;

public class DiscountStrategy {

    private byte discount = 1;

    public byte getDiscount(@Nullable User user, @Nonnull Event event, @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
        return discount;
    }

    public void setDiscount(byte discount) {
        this.discount = discount;
    }
}
