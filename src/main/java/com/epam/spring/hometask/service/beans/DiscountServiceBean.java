package com.epam.spring.hometask.service.beans;

import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.service.interfaces.DiscountService;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class DiscountServiceBean implements DiscountService{

    private List<DiscountStrategy> strategies = new ArrayList<>();

    @Override
    public byte getDiscount(@Nullable User user, @Nonnull Event event, @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
        byte totalDiscount = 0;
        for (DiscountStrategy strategy : strategies) {
            totalDiscount += strategy.getDiscount(user, event, airDateTime, numberOfTickets);
        }
        return totalDiscount;
    }
}
