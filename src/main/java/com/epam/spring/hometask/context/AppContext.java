package com.epam.spring.hometask.context;

import com.epam.spring.hometask.aspects.CounterAspect;
import com.epam.spring.hometask.aspects.DiscountAspect;
import com.epam.spring.hometask.aspects.LuckyWinnerAspect;
import com.epam.spring.hometask.dao.*;
import com.epam.spring.hometask.domain.Auditorium;
import com.epam.spring.hometask.domain.Event;
import com.epam.spring.hometask.domain.Ticket;
import com.epam.spring.hometask.domain.User;
import com.epam.spring.hometask.service.beans.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Configuration
public class AppContext {

    //Dao

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public IDao getUserDao(){
        return new UserDao();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public IDao getTicketDao(){
        return new TicketDao();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public IDao getEventDao(){
        return new EventDao();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public IDao getAuditoriumDao(){
        return new AuditoriumDao();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public IDao getDomainDao(){
        return new DomainObjectDao();
    }


    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public BookingDao getBookingDao(){
        return new BookingDao();
    }


    //Services

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public UserServiceBean getUserServiceBean(){
        return new UserServiceBean();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public AuditoriumServiceBean getAuditoriumServiceBean(){
        return new AuditoriumServiceBean();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public BookingServiceBean getBookingServiceBean(){
        return new BookingServiceBean();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public DiscountServiceBean getDiscountServiceBean(){
        return new DiscountServiceBean();
    }

    @Bean
    public EventServiceBean getEventServiceBean(){
        return new EventServiceBean();
    }

    //aspects

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public CounterAspect getCounterAspect(){
        return new CounterAspect();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public DiscountAspect getDiscountAspect(){
        return new DiscountAspect();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public LuckyWinnerAspect getLuckyWinnerAspect(){
        return new LuckyWinnerAspect();
    }

}
